// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  GEMINI_PRICE_URL: 'https://api.gemini.com/v1/pubticker/btcusd',
  hackcors: 'https://cors-anywhere.herokuapp.com/',
  BLOCK_API: '176f-b5c9-95c2-55d8',
  BLOCK_URL: 'https://block.io/api/v2/get_balance/?api_key=',
  BITTREX_API: '71453a1d2337412aadd78a5615655936',
  BITTREX_URL: 'https://bittrex.com/api/v1.1/',
  GDAX_BASE_URL: 'https://api.gdax.com',
  CC_URL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,EUR',
  CC_COINBASE_URL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD&e=Coinbase',
  CC_KRAKEN_URL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD&e=Kraken',
  CC_POLONIEX_URL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD&e=Poloniex',
  CC_BITFINEX_URL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD&e=Bitfinex',
  CC_GEMINI_URL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD&e=Gemini',
  CC_AVG_URL: 'https://min-api.cryptocompare.com/data/generateAvg?fsym=BTC&tsym=USD&markets=Coinbase,Gemini,Kraken',
  CC_kraken_eth: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD&e=Kraken',
  CC_coinbase_eth: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD&e=Coinbase',
  CC_gemini_eth: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD&e=Gemini',
  CC_AVG_ETH: 'https://min-api.cryptocompare.com/data/generateAvg?fsym=ETH&tsym=USD&markets=Coinbase,Gemini,Kraken'
};
