import { Component, OnInit, DoCheck } from '@angular/core';
import { MarketsService } from '../shared/markets.service';
import { Market } from '../market';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-markets',
  templateUrl: './markets.component.html',
  styleUrls: ['./markets.component.scss']
})

export class MarketsComponent implements OnInit, DoCheck {
  markets: Market[];
  errorMessage: string;
  geminiPrice: number;
  krakenPrice: number;
  coinbasePrice: number;
  error;
  prices;
  calculations = false;


  constructor(private marketService: MarketsService) { }

  ngOnInit() {
    this.getMarketAverage();
    this.getMarketPrices();
    IntervalObservable.create(120000)
      .subscribe(() => {
        this.marketService.getMarketPrices()
          .subscribe(
          data => {
            console.log(data);
            this.coinbasePrice = data[0];
            this.krakenPrice = data[1];
            this.geminiPrice = data[2];

          },
          error => this.errorMessage = <any>error);
      }
      );
    }

  ngDoCheck() {
    // this.prepareCalc();
    // this.sortPrices(this.prices);
  }

  prepareCalc() {
    this.calculations = true;
    const values = [this.coinbasePrice['USD'], this.krakenPrice['USD'], this.geminiPrice['USD']];
    values.sort();
    const range = (values[2] - values[0]);
    console.log('Range is $' + range);
      }

  sortPrices(prices) {
    prices = {
   'Coinbase': this.coinbasePrice['USD'],
   'Kraken': this.krakenPrice['USD'],
   'Gemini': this.geminiPrice['USD']

    };

    // let sortedValues = Object.keys(prices).sort((a, b) => prices[a] - prices[b]).reduce((_sortedObj, key) => ({ ..._sortedObj, [key]: prices[key] }), {});
    let sortedPrices = Object.keys(prices).sort((a, b) => prices[a] - prices[b]);
    let min = sortedPrices[0];
    let max = sortedPrices[2];
    console.log(sortedPrices);
    console.log('MIN is ' + min);
    console.log('MAX is ' + max);
}




  getMarketAverage() {
    this.marketService.getMarketAverage()
      .subscribe(
      markets => this.markets = markets,
      error => this.errorMessage = <any>error);
    // console.log(this.markets);
  }

  getMarketPrices() {
    this.marketService.getMarketPrices()
      .subscribe(
      data => {
        console.log(data);
        this.coinbasePrice = data[0];
        this.krakenPrice = data[1];
        this.geminiPrice = data[2];

      },
      error => this.errorMessage = <any>error);
  }


}








