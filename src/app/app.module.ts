import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// tslint:disable-next-line:max-line-length
import { MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule, MdGridListModule, MdSelectModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PriceService } from './shared/price.service';
import { MarketsService } from './shared/markets.service';
import 'hammerjs';
import { MarketsComponent } from './markets/markets.component';
import { PriceComponent } from './price/price.component';
import { NavComponent } from './nav/nav.component';
import { GoogleChartComponent } from './googlechart/googlechart.component';
import { BitcoinChartComponent } from './bitcoin-chart.component';

import { EthereumService } from './ethereum.service';

@NgModule({
  declarations: [
    AppComponent,
    MarketsComponent,
    PriceComponent,
    NavComponent,
    GoogleChartComponent,
    BitcoinChartComponent


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdMenuModule,
    MdCardModule,
    MdToolbarModule,
    MdIconModule,
    MdGridListModule,
    MdSelectModule
  ],
  providers: [PriceService, MarketsService, EthereumService],
  bootstrap: [AppComponent]
})
export class AppModule { }
