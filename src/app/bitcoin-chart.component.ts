import { Component } from '@angular/core';
import { GoogleChartComponent } from './googlechart/googlechart.component';



@Component({
  selector: 'app-bitcoin-chart',
  template: `
   <div id="google_barchart" style="width: 700px; height: 500px;"></div>
  `,
  styles: []
})
export class BitcoinChartComponent extends GoogleChartComponent {
  private options;
  data;
  private chart;
  krakenPrice: number;
  bitfinexPrice: number;
  poloniexPrice: number;
  coinbasePrice: number;
  errorMessage: string;



  drawGraph() {
    console.log('Drawing Bitcoin Graph');
    this.data = this.createDataTable([
     ['Price', 'Coinbase', 'Kraken', 'Gemini', 'CEX'],
     ['USD', 2319, 2325, 2318, 2345]
    ]);

    this.options = {
          title: 'Bitcoin Price',
          subtitle: 'Real time price data across exchanges',
          bars: 'vertical',
          hAxis: {
          title: 'The Current Price',
          minValue: 0
        }
    };

    this.chart = this.createBarChart(document.getElementById('google_barchart'));
    // this.chart.draw(this.data, this.options);
  }
}
