import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PriceComponent } from './price/price.component';
import { MarketsComponent } from './markets/markets.component';

const routes: Routes = [
  {
    path: 'price',
    component: PriceComponent,
    children: []
  },
  {
    path: 'markets',
    component: MarketsComponent
  },
  { path: '', pathMatch: 'full', component: MarketsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
