import { Component, OnInit, Input } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-googlechart',
  template: '<ng-content></ng-content>',
  styleUrls: ['./googlechart.component.scss']
})

export class GoogleChartComponent implements OnInit {
  private static googleLoaded: any;
  @Input() data;
  errorMessage: string;

  constructor() {
console.log('Here is GoogleChartComponent');
  }


  getGoogle() {
    return google;
  }

  ngOnInit() {
    console.log('ngOnInit Google Component');
    if (!GoogleChartComponent.googleLoaded) {
      GoogleChartComponent.googleLoaded = true;
      google.charts.load('current', {
        'packages': ['corechart']
      });
      google.charts.setOnLoadCallback(() => this.drawGraph());
    }

  }

  drawGraph() {
      console.log('DrawGraph base class!!!! ');
  }

  createBarChart(element: any): any {
      return new google.visualization.BarChart(element);
  }

  createDataTable(array: any[]): any {
      return google.visualization.arrayToDataTable(array);
  }
}
