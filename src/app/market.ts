export class Market {
  public Market: string;
  public From: string;
  public To: string;
  public Price: string;
  public Volume24Hour: string;
  public High24Hour: string;
  public Low24Hour: string;
  public Change24Hour: number;
  public ChangePct24Hour: number;
}
