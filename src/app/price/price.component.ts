import { Component, OnInit } from '@angular/core';
import { PriceService } from '../shared/price.service';


@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {
  priceBid: string;
  priceAsk: string;
  errorMessage: string;
  price;
  constructor(private priceService: PriceService) { }

  ngOnInit() {
    this.getPrice();
  }
  getPrice() {
    this.priceService.getPrice()
      .subscribe(
      price => this.price = price,

      // priceAsk => this.priceAsk = priceAsk.asks[0]
    );
    
  }
}

