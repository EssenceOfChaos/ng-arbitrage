import { Injectable } from '@angular/core';
import { Http, Response, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Market } from '../market';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class MarketsService {
  private CC_AVG_URL = environment.CC_AVG_URL;
  private coinbase = environment.CC_COINBASE_URL;
  private kraken = environment.CC_KRAKEN_URL;
  // private CC_POLONIEX_URL = environment.CC_POLONIEX_URL;
  // private CC_BITFINEX_URL = environment.CC_BITFINEX_URL;
  private gemini = environment.CC_GEMINI_URL;
  // private cexio = environment.CC_CEXIO_URL;


constructor(private _http: Http) { }

getMarketAverage() {
    return this._http.get(this.CC_AVG_URL).map(res => res.json());
  }


getMarketPrices() {
  return Observable.forkJoin(
    this._http.get(this.coinbase)
      .map((res: Response) => res.json()),
    this._http.get(this.kraken).map((res: Response) => res.json()),
    this._http.get(this.gemini).map((res: Response) => res.json())

  );


}

private handleError (error: Response | any) {
    // change logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
