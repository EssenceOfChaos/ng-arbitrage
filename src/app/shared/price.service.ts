import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()

export class PriceService {

  private gdax_url = environment.GDAX_BASE_URL;

  constructor(private _http: Http) { }

  getPrice() {
    return this._http.get(this.gdax_url + '/products/BTC-USD/book').map(res => res.json());
  }

}

