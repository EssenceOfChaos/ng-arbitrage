import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PriceService } from './shared/price.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  price: string;
  currency = 'USD';

  currencies = [
    { value: 'CNY', viewValue: 'CNY' },
    { value: 'AUD', viewValue: 'AUD' },
    { value: 'USD', viewValue: 'USD' },
    { value: 'EUR', viewValue: 'EUR' },
    { value: 'RUR', viewValue: 'RUR' }
  ];


handleSuccess(data) {
    this.price = data.hits;
    console.log(data.hits);
  }

  handleError(error) {
    console.log(error);
  }
  constructor(private _priceService: PriceService) { }



}
