import { NgArbitragePage } from './app.po';

describe('ng-arbitrage App', () => {
  let page: NgArbitragePage;

  beforeEach(() => {
    page = new NgArbitragePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
